package aed;

class Funciones {

    int cuadrado(int x) {
        int res = x*x;
        return res;
    }

    double distancia(double x, double y) {
        double res = Math.sqrt(x*x + y*y);
        return res;
    }

    static boolean esPar(int n) {
        boolean res= false;
        if ((n%2)==0){
            res = true;
        }
        return res;
    }
 
    boolean esBisiesto(int n) {
        boolean res = false;
        if (n%400 == 0){
            res = true;
        }
        else if ((n%4 == 0) && (n%100 != 0)){
            res = true;
        }
                
        return res;
    }

     int factorialIterativo(int n) {
        int i = 1;
        int res = 1;
        while (i <=n){
            res = res*i;
            i++;
        }

        return res;
    }

     int factorialRecursivo(int n) {
        int res = 1;
        if ((n != 1)&&(n!=0)){
            res = n*factorialRecursivo(n-1);
        }
        return res;
    }

     boolean esPrimo(int n) {
        boolean res = true;
        double i = 2;
        if (n==0 || n==1){
            res = false;
        }
        else
        while (i <= Math.sqrt(n)){
            if (n%i == 0){
                res = false;
            }
            i++;
        }
        return res;
    }

     int sumatoria(int[] numeros) {
        int i = 0;
        int res = 0;
        while ( i < numeros.length){
            res= res + numeros[i];
            i++;
        }
        return res;
    }

    int busqueda(int[] numeros, int buscado) {
        int i = 0;
        int res = -1;
        while (i< numeros.length){
            if (buscado == numeros[i]){
                res = i;
            }
            i++;
        }
        
        return res;
    }

    boolean tienePrimo(int[] numeros) {
        int i = 0;
        boolean res = false;
        while ( i < numeros.length){
            if (esPrimo(numeros[i])){
                res = true;
            }
            i++;
        }
        return res;
    }

    boolean todosPares(int[] numeros) {
        boolean res = true;
        int i = 0;
        while (i<numeros.length){
            if (!esPar(numeros[i])){
                res = false;
            }
            i++;
        }
        return res;
    }

    boolean esPrefijo(String s1, String s2) {
        boolean res = true;
        int i = 0;
        if (s2.length()<s1.length()){
            res = false;
        }
        else
        while (i<s1.length()){
            if ((s1.charAt(i)) != (s2.charAt(i))){
                res = false;
            }
            i++;
        }
        return res;
    }

    boolean esSufijo(String s1, String s2) {
        boolean res = true;
        int i = s2.length()-1;
        int j = s1.length()-1;
        if (s2.length()<s1.length()){
            res = false;
        }
        else
        while (i>=0 && j>=0){
            if ((s1.charAt(j)) != (s2.charAt(i))){
                res = false;
            }
            i--;
            j--;
        }
        return res;
    }
}
