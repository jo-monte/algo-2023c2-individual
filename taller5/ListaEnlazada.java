package aed;

public class ListaEnlazada<T> {
    private Nodo pri;
    private Nodo ult;
    private int longitud;

    private class Nodo {
        T valor;
        Nodo anterior;
        Nodo siguiente;

        public Nodo(T valor, Nodo anterior, Nodo siguiente) {
            this.valor = valor;
            this.anterior  = anterior;
            this.siguiente = siguiente;
        }
    }

    public ListaEnlazada() {
        pri = null;
        ult  = null;
        longitud = 0;
    }

    public int longitud() {
        return longitud;
    }

    public void agregarAdelante(T elem) {
        Nodo nuevo = new Nodo(elem, null, pri);
        if (longitud == 0) {
            pri = nuevo;
            ult  = nuevo; 
        } else {
            pri.anterior = nuevo;
            pri = nuevo;
        }
        longitud++;
    }

    public void agregarAtras(T elem) {
        Nodo nuevo = new Nodo(elem, ult, null);
        if (longitud == 0) {
            pri = nuevo;
            ult  = nuevo;
        } else {
            ult.siguiente = nuevo;
            ult = nuevo;
        }
        longitud++;
    }
    public T obtener(int i) {
        Nodo actual = pri;
        while(i>0){
            actual = actual.siguiente;
            i--;
        }
        return actual.valor;
    }

    public void eliminar(int i) {
        if (longitud == 1) {
            pri = null;
            ult  = null;
        } else if (i == 0){
            pri = pri.siguiente;
            pri.anterior = null;
        } else if (i == longitud-1) {
            ult = ult.anterior;
            ult.siguiente = null;
        } else {
            Nodo previo = pri;
            while (i>1) {
                previo = previo.siguiente;
                i--;
            }
            previo.siguiente = previo.siguiente.siguiente;
            previo.siguiente.anterior = previo;
        }
        longitud--;
    }

    public void modificarPosicion(int indice, T elem) {
        Nodo actual = pri;
        while(indice>0){
            actual = actual.siguiente;
            indice--;
        }
        actual.valor = elem;
    }

    public ListaEnlazada<T> copiar() {

        ListaEnlazada<T> listaCopia = new ListaEnlazada<T>();
        int i = 0;
        Nodo actual = pri;

        while (i<longitud) {
            listaCopia.agregarAtras(actual.valor);
            actual = actual.siguiente;
            i++;
        }

        return listaCopia;
    }
    
    public ListaEnlazada(ListaEnlazada<T> lista) {
        Iterador<T> it = lista.iterador();

        while (it.haySiguiente()) {
            this.agregarAtras(it.siguiente());
        }
    }
    
    @Override
    public String toString() {
        Nodo actual = pri;
        int indice  = longitud;
        String res  = "[";

        while(indice>1){
            res = res + actual.valor + ", ";
            actual = actual.siguiente;
            indice--;
        }
        res = res + actual.valor + "]";

        return res;
    }
    

    // O(1)
    // Agrega la lista parametro al final de esta lista
    public void unirListas(ListaEnlazada<T> lista) {
        if(lista.pri == null) {
            lista.pri = pri;
            lista.ult = ult;
        } else if (pri == null) {
            pri = lista.pri;
            ult = lista.ult;
        } else {
            ult.siguiente = lista.pri;
            lista.pri.anterior = ult;

            ult = lista.ult;
            lista.pri = pri;
        }
        this.longitud += lista.longitud;
    }


    public T[] lista2Array(){
        T[] res = (T[]) new Object[longitud()];
        Iterador<T> it = iterador();

        int i = 0;
        while (it.haySiguiente()) {
            res[i] = it.siguiente();
            i++;
        }
        return res;
    }

    private class ListaIterador implements Iterador<T> {
        int indice;

        public ListaIterador(){
            indice = 0;
        }

        public boolean haySiguiente() {
	        return indice < longitud;
        }
        
        public boolean hayAnterior() {
	        return indice > 0;
        }

        public T siguiente() {
            T res = obtener(indice);
            indice++;
	        return res;
        }
        
        public T anterior() {
            indice--;
            T res = obtener(indice);
	        return res;
        }
    }

    public Iterador<T> iterador() {
	    Iterador<T> it = new ListaIterador();
        return it;
    }

}
