package aed;

public class InternetToolkit {
    public InternetToolkit() {
    }

    public Fragment[] tcpReorder(Fragment[] fragments) {
        for (int i=1; i< fragments.length; i++) {
            int j = i-1;
            while(j >= 0 && fragments[j+1].compareTo(fragments[j])<0){
                Fragment pepe = fragments[j+1];
                fragments[j+1] = fragments[j];
                fragments[j] = pepe;
                j--;
            }
        }
        return fragments;
    }


    public Router[] kTopRouters(Router[] routers, int k, int umbral) {
        Router[] res = new Router[k];
        Heap heap = new Heap(routers);
        for (int i = 0; i<k;i++){
            if(heap.primero().getTrafico() >= umbral){
                res[i] = heap.sacarMaxReordenar();
            }
            
        }
        return res;
    }

    public IPv4Address[] sortIPv4(String[] ipv4) {
        IPv4Address[] resIPs = new IPv4Address[ipv4.length];
        for(int i=0; i<ipv4.length; i++) {
            resIPs[i] = new IPv4Address(ipv4[i]);
        }
        ListaEnlazada<IPv4Address>[] buckets = new ListaEnlazada[256];
        for(int i=3; i>=0; i--){
            for(int j=0; j<256; j++){
                buckets[j] = new ListaEnlazada<IPv4Address>();
            }
            for(int j=0; j<resIPs.length; j++) {
                IPv4Address ip = resIPs[j];
                buckets[ip.getOctet(i)].agregarAtras(ip);
            }
            ListaEnlazada<IPv4Address> listaTemp = new ListaEnlazada<IPv4Address>();
            for(int j=0; j<256; j++) {
                listaTemp.unirListas(buckets[j]);
            }
            
            Iterador<IPv4Address> iterador = listaTemp.iterador();
            int j = 0;
            while (iterador.haySiguiente()) {
                resIPs[j] = iterador.siguiente();
                j++;
            }
        }

        for(int i=0; i<resIPs.length; i++){
            System.out.println(resIPs[i].toString());
        }
        return resIPs;
    }
}
