package aed;

public class Heap {
    private Router[] elems;
    private int longitud;

    Heap(Router[] elementos){
        int n = elementos.length;
        for (int i = n / 2 - 1; i >= 0; i--) {
            heapify(elementos, n, i);
        }
        elems = elementos;
        longitud = elementos.length;
    }

    public void heapify (Router[]elementos, int n, int i){
        int largest = i;    // Initialize the largest as the root
        int left = 2 * i + 1;
        int right = 2 * i + 2;

        // If left child is larger than root
        if (left < n && elementos[left].compareTo(elementos[largest]) ==1) {
            largest = left;
        }

        // If right child is larger than the largest so far
        if (right < n &&elementos[right].compareTo(elementos[largest]) ==1) {
            largest = right;
        }

        // If the largest is not the root
        if (largest != i) {
            // Swap the root with the largest element
            Router temp = elementos[i];
            elementos[i] = elementos[largest];
            elementos[largest] = temp;

            // Recursively heapify the affected subtree
            heapify(elementos, n, largest);
        }
    }
    public Router sacarMaxReordenar (){
        Router max = elems[0];
        reOrdenar();
        longitud--;
        return max;
    }
    public void reOrdenar(){
        Router nuevo = elems[longitud-1];
        elems[0] = nuevo;
        acomodarRecursivo(0);
    }

    public void acomodarRecursivo(int pos) {
        Router padre = elems[pos];
        if (pos * 2 + 1 < longitud && pos * 2 + 2 < longitud) {
            Router hijo = elems[pos * 2 + 1];
            Router hermano = elems[pos * 2 + 2];
            if ((hijo.compareTo(hermano)>=0) && (hijo.compareTo(padre)==1)) {
                elems[pos] = hijo;
                elems[pos * 2 + 1] = padre;
                acomodarRecursivo(pos * 2 + 1);
            } else if ((hermano.compareTo(hijo)>=0) && (hermano.compareTo(padre)==1)) {
                elems[pos] = hermano;
                elems[pos * 2 + 2] = padre;
                acomodarRecursivo(pos * 2 + 2);
            }
        } else if (pos * 2 + 1 < longitud && pos * 2 + 2 >= longitud) {
            Router hijo = elems[pos * 2 + 1];
            if (hijo.compareTo(padre)==1) {
                elems[pos] = hijo;
                elems[pos * 2 + 1] = padre;
            }
        }
    }
    public Router primero (){
        return elems[0];
    }
}
