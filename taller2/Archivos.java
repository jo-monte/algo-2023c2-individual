package aed;

import java.util.Scanner;
import java.io.PrintStream;

class Archivos {
    float[] leerVector(Scanner entrada, int largo) {
        float [] pepito = new float[largo];
        int i = 0;
        while (i<largo){
            pepito[i] = entrada.nextFloat();
            i++;
        }
        return pepito;
    }

    float[][] leerMatriz(Scanner entrada, int filas, int columnas) {
        float[][] pepito = new float[filas][columnas];
        int i = 0;
        while (i<filas){
            pepito[i] =leerVector(entrada, columnas);
            i++;
        }
        return pepito;
    }

    void imprimirPiramide(PrintStream salida, int alto) {
        int i = 1;
        while (i<alto+1){
            imprimirEspacio(salida, alto-i);
            imprimirAsterisco(salida, i*2-1);
            imprimirEspacio(salida, alto-i-1);
            if (i!=alto){
                salida.println(" ");
            }
            i++;
        }
    }
    void imprimirEspacio (PrintStream salida, int veces){
        int i = 0;
        while (i<veces){
            salida.print(" ");
            i++;
        }
    }
    void imprimirAsterisco (PrintStream salida, int veces){
        int i = 0;
        while (i<veces){
            salida.print("*");
            i++;
        }
    }
}