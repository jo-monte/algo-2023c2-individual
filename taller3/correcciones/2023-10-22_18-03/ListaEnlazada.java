package aed;

import java.util.*;

public class ListaEnlazada<T> implements Secuencia<T> {
    private int longitud; 
    private Nodo pri;
    private Nodo ult;

    private class Nodo {
        T v;
        Nodo sig;
        Nodo ant;
        
        Nodo(T valor){
            v = valor; 
            sig = null; 
            ant = null;
        }

    }

    public ListaEnlazada() {
        longitud = 0;
        pri = null;
        ult = null;
    }

    public int longitud() {
        return longitud;
    }

    public void agregarAdelante(T elem) {
        Nodo nuevo = new Nodo(elem);
        
        if (ult == null){
            pri = nuevo;
            ult = nuevo;
        }
        else{
            nuevo.sig = pri;
            pri.ant = nuevo;
            pri = nuevo;
        }
        longitud = longitud +1;
    }

    public void agregarAtras(T elem) {
        Nodo nuevo = new Nodo(elem);
        if (pri == null){
            pri = nuevo;
            ult = nuevo;
        } else{
            nuevo.ant = ult;
            ult.sig = nuevo;
            ult = nuevo;
        }
        longitud = longitud +1;
    }
        

    public T obtener(int i) {
        Nodo actual = pri;
        int j = 0;
        while (j<i){
            actual = actual.sig;
            j++;
        }
        return actual.v;
    }

    public void eliminar(int i) {
        Nodo actual = pri;
        int j = 0;
        while (j<i){
            actual = actual.sig;
            j++;
        }
        if (longitud == 1){
            pri = null;
            ult = null;
        }else {
            if (i == longitud -1 ){
                ult = actual.ant; 
                ult.sig = null;
            }
            else{
                if (i == 0) {
                    actual.sig.ant = null;
                    pri = actual.sig;
                } 
                else{
                    actual.ant.sig = actual.sig;
                    actual.sig.ant = actual.ant;
                    
                }
            }  
        }
        longitud = longitud-1;
        
    }

    public void modificarPosicion(int indice, T elem) {
        Nodo actual = pri;
        int j = 0;
        while (j<indice){
            actual = actual.sig;
            j++;
        }
        actual.v = elem;
    }

    public ListaEnlazada<T> copiar() {
        ListaEnlazada<T> nuevo = new ListaEnlazada<T>();
        Nodo actual = ult;
        int i = 0;

        while (i < longitud){
            nuevo.agregarAdelante(actual.v);
            actual = actual.ant;
            i++;
        }

        return nuevo;

    }

    public ListaEnlazada(ListaEnlazada<T> lista) {
        int i = 0;
        pri = null;
        ult = null;
        Nodo actual = lista.ult;
        while (i < lista.longitud){
            agregarAdelante(actual.v);
            actual = actual.ant;
            i++;
        }
    }
    
    @Override
    public String toString() {
        Nodo actual = pri;
        int j = 0;
        String res = "[";
        while (j<longitud){
            if (j < longitud-1){
                res = res + actual.v + ", ";
                actual = actual.sig;
            }
            else {res = res + actual.v;}
            j++;
        }
        return res+"]";
    }

    private class ListaIterador implements Iterador<T> {
        int dedito;
        ListaIterador(){
            dedito = 0;
        }

        public boolean haySiguiente() {
	        return dedito < longitud;
        }
        
        public boolean hayAnterior() {
            return dedito != 0;
        }

        public T siguiente() {
            T res =  obtener(dedito);
            dedito = dedito +1;
            return res;
        }

        public T anterior() {
            dedito = dedito-1;
            return obtener(dedito);
        }
    }

    public Iterador<T> iterador() {
        return new ListaIterador();
    }

}
