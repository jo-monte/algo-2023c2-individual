
package aed;

import java.util.*;

// Todos los tipos de datos "Comparables" tienen el método compareTo()
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    private Nodo raiz;
    private int cardinal;


    private class Nodo {
        Nodo izq;
        Nodo der;
        Nodo padre;
        T v;

        Nodo(T elem){
            izq = null;
            der = null;
            padre = null;
            v = elem;
        }
        }


    private boolean esMayor(T elem, T v){
        return (elem.compareTo(v) > 0);
    }
    private boolean esMenor(T elem, T v){
        return (elem.compareTo(v) < 0);
    }
    private boolean derechaVacia(Nodo actual){
        return (actual.der == null);
    }
    private boolean izquierdaVacia(Nodo actual){
        return (actual.izq == null);
    }
    private boolean soyEste(T elem, Nodo actual){
        boolean res = false;
        if (actual != null){
            res = (actual.v == elem);
        } 
        return res;
    }
    private boolean tieneDosHijos(Nodo actual){
        return actual.izq != null && actual.der != null; 
    }
    private boolean tieneUnHijos(Nodo actual){
        return actual.izq != null || actual.der != null; 
    }
    private boolean noTieneHijos(Nodo actual){
        return actual.izq == null && actual.der == null; 
    }
    public Nodo hallarElem (T elem){
        Nodo actual = raiz;
        if (raiz==null){
            return raiz;
        }
        int i = 0;
        while (i<cardinal && actual.v!= elem){
            if (esMayor(elem, actual.v)&&!derechaVacia(actual)){
                actual = actual.der;
            }
            else{
                if(esMenor(elem, actual.v)&&!izquierdaVacia(actual)){
                    actual = actual.izq;
                }
            }
            i++;
        }
        return actual;
    }
    public T mostra (T elem){
        return hallarElem(elem).v;
    }
    
    
    private String stringeameAux(){
        T valor = minimo();
        eliminar(valor);
        if (cardinal == 0){
            return valor+"}";
        }
        else{return valor+","+stringeameAux();}
    }

    private String stringueame (){
        ABB<T> copia = copiar();
        return copia.stringeameAux();
    }
    private T hallarMayorDeLosMenores(Nodo pepe){
        Nodo actual = pepe.izq;
        while (actual.der != null){
            actual = actual.der;
        }
        return actual.v;
    }

    private Nodo elMinimo (Nodo pepe){
        if (pepe.izq != null){
            return elMinimo(pepe.izq);
        }else{
            return pepe;
        }
    }
    public ABB<T> copiar(){
        ABB<T> nuevoArbool = new ABB();
        nuevoArbool.insertar(raiz.v);
        nuevoArbool.raiz.der = raiz.der;
        nuevoArbool.raiz.izq = raiz.izq;
        nuevoArbool.raiz.der.padre = nuevoArbool.raiz;
        nuevoArbool.raiz.izq.padre = nuevoArbool.raiz;
        nuevoArbool.cardinal = cardinal;
        return nuevoArbool;
    }
    

    public ABB() {
        raiz = null;
        cardinal = 0;
    }
    public T raiz(){
        return raiz.v;
    }

    public int cardinal() {
        return cardinal;
    }

    public T minimo(){
        Nodo actual = raiz;
        while (actual.izq != null){
            actual = actual.izq;
        }
        return actual.v;
    }

    public T maximo(){
        Nodo actual = raiz;
        while (actual.der != null){
            actual = actual.der;
        }
        return actual.v;
    }

    public void insertar(T elem){
        int i = 0;
        Nodo actual = raiz;
        Nodo nuevo = new Nodo(elem);
        Integer aaaaaa = cardinal;

        while (i<aaaaaa){
            if (esMayor(elem, actual.v)){
                if (derechaVacia(actual)){
                    actual.der = nuevo;
                    nuevo.padre = actual;
                    cardinal = cardinal+1;
                }
                else{
                    actual = actual.der;
                }
            }
            else{
                if(esMenor(elem, actual.v)){
                    if (izquierdaVacia(actual)){
                        actual.izq = nuevo;
                        nuevo.padre = actual;
                        cardinal = cardinal+1;
                        }
                    else{
                    actual = actual.izq;
                    }
                }
            }
            i++;
        }
        if (raiz == null){
            raiz = nuevo;
            cardinal++;
        }
    }

    public boolean pertenece (T elem){
        if(raiz == null){
            return false;
        }
        else{
           return perteneceAux(raiz, elem);
        }
    }
    public boolean perteneceAux (Nodo pepe, T elem){
        if (pepe == null){
            return false;
        }
        else if (elem.compareTo(pepe.v)== 0){
            return true;            
        }else{
            return perteneceAux(pepe.der,elem) || perteneceAux(pepe.izq,elem);
        }
    }

    public void eliminar(T elem){
        if(pertenece(elem)){

            Nodo pepe = hallarElem( elem);
            if (noTieneHijos(pepe)){
                if (pepe.padre == null){
                    raiz = null;
                }
                else{
                    if(pepe.padre.izq == pepe){
                        pepe.padre.izq = null;
                    }
                    if(pepe.padre.der == pepe){
                        pepe.padre.der = null;
                    }
                }
            }
            if(tieneUnHijos(pepe) && derechaVacia(pepe)){
                if (pepe.padre == null){
                    raiz = pepe.izq;
                    pepe.izq.padre = null;
                }else{
                    if(pepe.padre.izq == pepe){
                        pepe.padre.izq = pepe.izq;
                        pepe.izq.padre = pepe.padre;
                    }else
                    if(pepe.padre.der == pepe){
                        pepe.padre.der = pepe.izq;
                        pepe.izq.padre = pepe.padre;
                    }
                }
            }else
            if(tieneUnHijos(pepe) && izquierdaVacia(pepe)){
                if (pepe.padre == null){
                    raiz = pepe.der;
                    pepe.der.padre = null;
                }else{
                    if(pepe.padre.izq == pepe){
                        pepe.padre.izq = pepe.der;
                        pepe.der.padre = pepe.padre;
                    }
                    if(pepe.padre.der == pepe){
                        pepe.padre.der = pepe.der;
                        pepe.der.padre = pepe.padre;
                    }
                }
            }else
            if(tieneDosHijos(pepe)){
                T valor = hallarMayorDeLosMenores(pepe);
                eliminar(valor);
                cardinal=cardinal+1;
                pepe.v = valor;
            }
            cardinal = cardinal-1;
        }
    }

   

    public String toString(){
        String res = "{";
        Iterador <T> te_itero = iterador();
        while(te_itero.haySiguiente()){
            T pepin = te_itero.siguiente();
            if (pepin != maximo()){
            res = res+pepin+",";
            }else{res = res+pepin+"}";}
        }
        return res;
    }

    private class ABB_Iterador implements Iterador<T> {
        private Nodo _actual;

        public ABB_Iterador(){
            _actual = null;
        }

        public boolean haySiguiente() { 
            if (_actual == null){
                return true;
            }           
            return _actual.v != maximo();
        }
    
        public T siguiente() { //SIEMPRE ASUMI QUE LOS MENORES YA LOS PASASTE
            Nodo pepe = _actual;
            if (pepe == null){
                _actual = hallarElem(minimo());
                return _actual.v;
            } else if (!derechaVacia(pepe)){
                pepe = elMinimo(pepe.der);
            } else {
                Nodo v_padre = pepe.padre;
                while(esMayor(pepe.v,v_padre.v)){
                    v_padre = v_padre.padre;
                }
                pepe = v_padre;
            }
            _actual = pepe;
            return pepe.v;
        }
    }

        public Iterador<T> iterador() {
            return new ABB_Iterador();
        }

    

}